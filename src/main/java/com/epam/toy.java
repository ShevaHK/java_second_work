package com.epam;

public class toy {
    private String color;
    private String name;
    private double size;

    public toy(String color,String name, double size) {
        this.name = name;
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }
}
