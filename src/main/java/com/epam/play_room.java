package com.epam;

public class play_room {
    private String ages;
    private int sum_money_for_room;
    private int count_of_toy;
    private toy[] toys;

    public toy[] getToys() {
        return toys;
    }


    public play_room(String ages, int sum_money_for_room, int count_of_toy, toy[] toys) {
        this.ages = ages;
        this.sum_money_for_room = sum_money_for_room;
        this.count_of_toy = count_of_toy;
        this.toys = new toy[count_of_toy];
    }

    public int getSum_money_for_room() {
        return sum_money_for_room;
    }

    public void setSum_money_for_room(int sum_money_for_room) {
        this.sum_money_for_room = sum_money_for_room;
    }

    public String getAges() {
        return ages;
    }

    public void setAges(String ages) {
        this.ages = ages;
    }

    public int getCount_of_toy() {
        return count_of_toy;
    }

    public void setCount_of_toy(int count_of_toy) {
        this.count_of_toy = count_of_toy;
    }
}
